# 21DTHD4-2180608631-VDT
Vũ Đức Thịnh
21DTHD4
2180608631 <br/>
| Title | Manager search for a product using ID |
| :---: |---| 
| Value statement | As a manager , I wanna find a necessary product item |
| Acceptance criteria | <ins> Acceptance criterion 1:</ins><br/> When I wanna find a product, I will enter the product's ID then I will see the product's information that has the input's ID.  |<br/>
| Definition of Done| <br/>Unit Tests Passed, <br/> Accetance Criteria Met, <br/> Code Reviewed, <br/> Functional Tests Passed, <br/> Non-Functional Requirements Met, <br/> Product Owner Accepts User Story|
|Owner| Vu Duc Thinh|
|UI |![Manager search for a product using ID](https://gitlab.com/thinhpro20313/21dthd4-2180608631-vdt/-/raw/main/Manager%20search%20for%20a%20product%20using%20ID.png?ref_type=heads)

| n | req id| Test objected | Text step | Expected result | 
|:---:|:---:|:---:|:---:|:---:| 
| 1 | TC01 | Verify that the manager can search for a product using ID |1. The manager enters the product ID in the search bar.<br/> 2. The manager clicks the search button  | 1.The search results page should be displayed.<br/> 2.The product with the specified ID should be listed in the search results.| 
| 2 | TC02 | Verify that the manager can search for a product using ID even if the ID is case-insensitive. | 1. The manager enters the product ID in the search bar in mixed case."<br/> 2. The manager clicks the search button.| 1.The search results page should be displayed.<br/>2.The product with the specified ID should be listed in the search results, regardless of the case of the ID entered in the search bar.|
| 3 | TC03 | Verify that the manager cannot search for a product using an invalid ID. |1. The manager enters the invalid product ID in the search bar.<br/> 2. The manager clicks the search button.| 1. An error message should be displayed, indicating that the product ID is invalid. | 





Đỗ Trọng Tín 2180608101 21DTHD4
|Title| Manager monthly sales statistics |
| ---| --- |
| Value Statement | As a manager I want a monthly sales statistics|
| Acceptance Criteria | <ins>Acceptance Criteria 1:<br/>Given that the manager entered a specific month and year <br/> Then the desktop will show the sales statistics that has the  month and year similar to the input  <br/> And ensure that the chosen month and year sales statistics is calculated correctly|
|Definition Of Done| Unit Tests Passed <br/>Acceptance Criteria Met <br/>Code Reviewed <br/>Functional Tests Passed <br/>Non-Functional Requirements Met<br/> Product Owner Accepts User Story|
| Owner | Đỗ Trọng Tín |
| Interation | Unschedules |
|UI |![Manager search for a product using ID](https://gitlab.com/dotrongtin/21dthd4-2180608101/-/raw/main/image.png)

| Number |  Req ID |                             Test Objective                             |                                                                                      Test steps                                                                                      |                                            Expected Result                                           | Actual Result | Pass/Fail | Related Defects |
|:------:|:-------:|:----------------------------------------------------------------------:|:------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------:|:----------------------------------------------------------------------------------------------------:|:-------------:|:---------:|:---------------:|
|    1   | REQ-001 | Viewing the similar month sales  statistics with only month input      | 1. Choose the wanted month from the month combo box 2. Click on "Show" button 3. Confirm by click "Yes" or "No" on the message box                                                   | Display month sales statistics  that similar to the chosen month  after confirmed "Yes"              |               |           |                 |
|    2   | REQ-002 | Viewing the similar year sales  statistics with only year input        | 1. Choose the wanted year from the year combo box 2. Click on "Show" button 3. Confirm by click "Yes" or "No" on the message box                                                     | Display month sales statistics  that had the year similar to the  chosen year after confirmed "Yes"  |               |           |                 |
|    3   | REQ-003 | Viewing the specific month sales  statistics with month and year input | 1. Choose the wanted month from the month combo box 2. Choose the wanted year from the year combo box 3. Click on "Show" button 4. Confirm by click "Yes" or "No" on the message box | Display tha month sales statistics  that had the chosen month and year  after confirmed "Yes"        |               |           |                 |


Nguyễn Tấn Sang 
2180607960
21DTHD4

|Title| Manager Add product information|
|:---:|:---:|
|Value statement|As a manager , I want add product items|
|Acceptance Criteria| Acceptance Criterion : Show production information in the system when manager has successfully entered production information into the system |
|Definition of Done| Unit Tests Passed <br/> Acceptance Criteria Met <br/>Code Reviewed <br/>Functional Tests Passed <br/>Non-Functional Requiremets Met <br/>Product owner accept user story|
|Owner| Nguyen Tan Sang   |
|UI |![Manager search for a product using ID](https://gitlab.com/fsd7321936/21dthd4-2180607960/-/raw/main/Manager%20Add%20product%20information.png)

| n | req id| Test objected | Text step | Expected result | 
|:---:|:---:|:---:|:---:|:---:| 
| 1 | REQ-015 | Test case 1: Add new product with all valid information.|Step 1: Log in to the system with an administrator account.<br/>Step 2: Access the Management menu > Products.<br/>Step 3: Click the Add new product button.<br/>Step 4: Enter all valid product information, including: product code, product name, product price, inventory, and other information if any.<br/>Step 5: Click the Save button.  | show product message "success"|
| 2 | REQ-016 | Test case 2: The new product is successfully created and displayed in the product list.|Step 1: Log in to the system with an administrator account.<br/>Step 2: Access the Management menu > Products.<br/>Step 3: Click the Add new product button.<br/>Step 4: Enter some product information, but some important information is missing, such as product name or product price.<br/>Step 5: Click the Save button. | Update failed.|
| 3 | REQ-017 | Adding a product failed because a product already had a transaction .|Step 1: Log in to the system with an administrator account.<br/>Step 2: Access the Management menu > Products.<br/>Step 3: Click the Add new product button.<br/>Step 4: Enter invalid product information, such as negative product price or negative inventory quantity.<br/>Step 5: Click the Save button.|The system displays an error message and does not create new products.|

﻿# 21DTHD4_2180601350
## Nguyen Tuan Tai 
## 2180601350


|Title| Manager Delete product information|
|:---:|:---:|
|Value statement|As a manager , I want delete product items|
|Acceptance Criteria| Select the product name to enter , delete product name from the system, aallow manager to select multiple products to delete at once, ddisplay a warning message to manager before deleting a product, aallow manager to restore deleted products.|
|Definition of Done| Unit Tests Passed <br/> Acceptance Criteria Met <br/>Code Reviewed <br/>Functional Tests Passed <br/>Non-Functional Requiremets Met <br/>Product owner accept user story.|
|Owner| Nguyen Tuan Tai   |
|iteration|  |
|Estimate| |
|UI |![Manager search for a product using ID](https://gitlab.com/NguyenTuanTai_2180601350_21htd4/21dthd4_2180601350/-/raw/main/Manager%20Delete%20product%20information.png?ref_type=heads)

| n | req id| Test objected | Text step | Expected result | 
|:---:|:---:|:---:|:---:|:---:| 
| 1 | REQ-015 | Successfully delete a product |1. Log in to the system with an administrator account.<br/> 2. Go to the menu Manage > Products.<br/> 3. Select the product to delete.<br/> 4. Tap the Delete button.<br/> 5. Confirm the removal of the product. | 1.The product is removed from the system.<br/> 2.The product no longer appears in the product list.<br/> 3. Users can't access deleted product information.  | 
| 2 | REQ-016 | Product deletion failures due to inventory not being 0 |1. Log in to the system with an administrator account.<br/> 2. Go to the menu Manage > Products.<br/> 3. Select the product to delete.<br/> 4. Tap the Delete button.<br/> 5. Confirm the removal of the product. | 1. The system displays the error message "The product has an inventory quantity that is not zero, cannot be deleted."<br/> 2. The product must not be removed from the system. | 
| 3 | REQ-017 | Deleting a product failed because a product already had a transaction |1. Log in to the system with an administrator account.<br/> 2. Go to the menu Manage > Products.<br/> 3. Select the product to delete.<br/> 4. Tap the Delete button.<br/> 5. Confirm the removal of the product. | 1. The system displays the error message "The product has already generated transactions, cannot be deleted."<br/> 2. The product must not be removed from the system. |

 Họ tên: Lưu Hoàng Anh Khoa.
 Lớp: 21DTHD4.
 MSSV: 2180607640.

| Title | Manage Update Product Information |
| :--:  | :-------------------------------: |
| Value Statement | As a manager, i want to edit the information about the products|
| Acceptence Criteria | Acceptence Criterion:Given the products if it are existed,Check the manager authority ,Fixable Properties,Overwrite saving|
| Definition Of Done | Unit Tests Passed,<br/>Acceptence Criteria Met,<br/>Code Reviewed,<br/>Functional Tests Passed,<br/>Non-Functional Requirements Met,<br/>Product Owner Accepts User Story |
|Owner| Luu Hoang Anh Khoa|
|UI |![Manager search for a product using ID](https://gitlab.com/buimanhtoan.it1/21dthd4-2180607640/-/raw/main/image3.png)

| No. | Test Case ID | Test Obj | Test Steps | Expected Result |
|---|---|---|---|---|
| 1 | TC01 | Verify that a manager can update the product name. | 1)The manager enters a new product name in the "Product Name" field.<br>2)The manager clicks the "Update" button. | The product name is updated successfully. |
| 2 | TC02 | Verify that a manager can update the product description. | 1)The manager enters a new product description in the "Product Description" field.<br>2)The manager clicks the "Update" button. | The product description is updated successfully. |
| 3 | TC03 | Verify that a manager can update the product price. | The manager enters a new product price in the "Product Price" field.<br>The manager clicks the "Update" button. | The product price is updated successfully.<br>These are just a few examples of test cases that can be written for the user story "Manage Update Product Information". More test cases can be written to cover different scenarios, such as:<br><br>Updating multiple product fields at the same time<br>Updating product fields with invalid values<br>Updating product fields for products that have already been ordered or sold |

